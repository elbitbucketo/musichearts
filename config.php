<?php

  // This file holds any configuration that can be adjusted by users of musicloads
  // The configurations are stored in variables and can easily be changed
  // This file is needed for musichearts to work, but can be left unchanged

 

  //////////////////
  // payment options
 
  // Leave blank if paypal is not to be supported
  $musichearts_paypal_username   = '__FILL_ME__';
  $musichearts_paypal_auth_token = '__FILL_ME__';
  $musichearts_paypal_ssl_mode   = false; //SSL modus is untested. Test before use.
 


  //////////////////
  // Basic Options
  
  // Band name and Email 
  $musichearts_band_name  = '__FILL_ME__';
  $musichearts_band_email = '__FILL_ME__@__FILL__.__ME__';
  
  // Price and Currency for one song. Will be configurable per song later.
  // For a first release, just one price will be possible. Check
  // https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_currency_codes
  // for supported currency codes
  $musichearts_song_currency = 'EUR';
  $musichearts_song_price    = '0.50';

  // Indicates whether to make a complete html page with header and body tags.
  // Set to 'false' if code is embedded into given webpage
  // See INSTALL.TXT fpr further information
  // using php command: include 'index_musichears.php';
  $musichearts_complete_html_page   = true; // set to true or false (no quotation)
 
  // amount of days, redownloads in the same browser will be allowed by having set a cookie.
  // set to 0 to diable
  $musichearts_cookie_download_time = 7;
  
  // Set this to 'true' if installation is done
  $musichearts_install_finished = false; // set to true or false (no quotation)
  

  ////////////////////
  // Outline options
  
  // width of whole plugin
  $musichearts_plugin_width       = '90%'; // relative width of the plugin
  
  // color for the plugin background, text and table-lines
  $musichearts_background_color   = '#ffffff'; //white
  $musichearts_color              = '#000000'; //black
  $musichearts_border_color       = '#000000'; //black
  $musichearts_link_color         = '#000099'; //somewhat blue
  
  // text font
  $musichearts_font               = 'normal 85% "Lucida Grande", Lucida, Verdana, sans-serif;';
  // a more female font, just comment out if you prefer this.
  // $musichearts_font               = 'normal 85% "Papyrus color", Papyrus, Verdana, sans-serif;';

  // You do not like the nice logo? turn it off
  $musichearts_show_logo = true; // set to true or false (no quotation)


  ////////////////////////////////
  // internationalization options

  // default language if browser detection fails
  $musichearts_default_language = 'en'; // up to now supporting 'de' and 'en'
 

  ////////////////////////////////
  // compatibilty options
  
  // use '\n' instead of '\r\n' in php mail headers. This is not correct according to RFC but
  // some "poor" mail sending programs are said to require it
   $musichearts_non_rfc_mail_headers = false; // set to true or false (no quotation)
  

  ////////////////////////////////
  // extended options

  // directory where the musicfiles are stored (relatively to basedir)
  $musichearts_musicfiles_dirname         = 'mp3';
  $musichearts_musicfiles_preview_dirname = 'mp3_preview';

  // use apache licensed animated preview buttons
  // TODO true for preview animated value needs flash. Noone wants this today. => remove deps
  // $musichearts_use_animated_preview       = false; // set to 'true' or 'false' (no quotation)


  /////////////////////
  // Developer options

  // use development settings
  // i.e. send calls do sanbox.paypal.com instead of paypal.com
  $musichearts_development_mode       = false; // set to true or false (no quotation)
  // Set also this, to require no payment at all
  $musichearts_development_mode_local = false; // set to true or false (no quotation)
  

?>
