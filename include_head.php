    <link rel="stylesheet" type="text/css" href="<?php echo @constant('musichearts_root_dir'); ?>css/basic.css.php" />
    
    <?php if( !isset( $_GET['musichearts_noscript'] )) { ?>
      
      <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/utils/popup.js" > 
      </script>   
      
      <?php if( !isset( $_GET['musichearts_payment'] )) { ?>
        
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/basket/ajax_basket_handler.js" >
        </script>
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/hacks/opera.js" >
        </script>
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/email/scrambler.js" >
        </script>
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>sndman/script/soundmanager2-nodebug-jsmin.js">
        </script>
        
        <link rel="stylesheet" type="text/css" href="<?php echo @constant('musichearts_root_dir'); ?>sndman/css/inlineplayer.css" />
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>sndman/script/inlineplayer.js" >
        </script>
        
        <script type="text/javascript">
          soundManager.url = '<?php echo @constant('musichearts_root_dir'); ?>sndman/swf/';
        </script>
      
      <?php } else { ?>
        
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/payment/form_submitter.js" > 
        </script>
        <script type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/payment/check_email.js" > 
        </script>
      
      <?php } ?>
    
    <?php } ?>
