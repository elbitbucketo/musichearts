<?php

// definition of texts used in musichearts


/* 

TEMPLATE:
$musichearts_text1 = array(
  "en" => "<englisch_text>",
  "de" => "<german_text>"
)


NOTE: Carriage returns and linebreaks ('\r and' '\n'should not be used. They can break Javascript Code.
      If used nevertheless it has to be ensured, the text is not used in a context where it can break things.

*/     

// Fallback for missing texts/translations
$musichearts_text_missing = array(
  "en" => "WARNING: MISSING TEXT",
  "de" => "WARNUNG: FEHLENDER TEXT"
);


// used texts
$musichearts_text_shop = array(
  "en" => "audio web shop",
  "de" => "musik webladen"
);

$musichearts_text_title_download = array(
  "en" => "MUSICHEARTS AUDIO WEB SHOP - DOWNLOAD",
  "de" => "MUSICHEARTS MUSIK WEBLADEN - HERUNTERLADEN"
);

$musichearts_text_download_songs = array(
  "en" => "My songs - click songs to download",
  "de" => "Meine Lieder - Lieder zum herunterladen anklicken"
);

$musichearts_text_song = array(
  "en" => "Song",
  "de" => "Lied"
);

$musichearts_text_price = array(
  "en" => "Price",
  "de" => "Preis"
);

$musichearts_text_get = array(
  "en" => "get it!",
  "de" => "will ich..."
);

$musichearts_text_preview = array(
  "en" => "preview",
  "de" => "Vorschau"
);

$musichearts_text_unget = array(
  "en" => "unget it!",
  "de" => "doch nicht..."
);

$musichearts_text_pay = array(
  "en" => "Pay",
  "de" => "Bezahlen"
);

$musichearts_text_sum = array(
  "en" => "Sum",
  "de" => "Summe"
);

$musichearts_text_download = array(
  "en" => "Download",
  "de" => "Herunterladen"
);

$musichearts_text_back = array(
  "en" => "BACK",
  "de" => "ZUR&Uuml;CK"
);

$musichearts_text_back_to_shop = array(
  "en" => "Back to shop",
  "de" => "Zur&uuml;ck zum shop"
);

$musichearts_text_paypal = array(
  "en" => "Pay with paypal",
  "de" => "Zahlen mit Paypal"
);

$musichearts_text_amount_to_pay = array(
  "en" => "Amount to pay:",
  "de" => "Zu zahlender Betrag:"
);

$musichearts_text_song_list = array(
  "en" => "Ordered songs:",
  "de" => "Bestellte Lieder:"
);

$musichearts_text_no_songs = array(
  "en" => "Sorry. You have no songs available to download",
  "de" => "Sie haben leider keine Lieder zum Download zur verf�gung."
);

$musichearts_text_cannot_purchase = array(
  "en" => "Sorry. Your purchase could not be made!",
  "de" => "Ihre Bestellung konnte leider nicht abgeschlossen werden."
);

$musichearts_text_error_basket = array(
  "en" => "ERROR: No Song in Basket. Maybe you lost your session?",
  "de" => "FEHLER: Kein Song im Warenkorb. Session verloren?"
);

$musichearts_text_choose_payment = array(
  "en" => "Please choose your payment method.",
  "de" => "Bitte w�hlen Sie ihre Zahlungsweise."
);

$musichearts_text_submit_payment = array(
  "en" => "Init payment",
  "de" => "Bezahlvorgang starten"
);

$musichearts_text_thank_you_big = array(
  "en" => "THANK YOU!",
  "de" => "DANKE!"
);

$musichearts_text_notify_pay = array(
  "en" => "Please feel free to shop on or checkout at shop bottom.",
  "de" => "Sie k&ouml;nnen nun weiter einkaufen oder am unteren Ende des Shops zur Kasse gehn."
);

$musichearts_text_empty_basket = array(
  "en" => "You have nothing in your basket. You session may have expired.",
  "de" => "Sie haben nichts in Ihrem Warenkorb. M&ouml;glicherweise is Ihre Sitzung abgelaufen."
);

$musichearts_text_back_to_shop = array(
  "en" => "Back to shop to continue shopping...",
  "de" => "Zur&uuml;ck zum Laden um weiter einzukaufen..."
);

$musichearts_text_redownload_link = array(
  "en" => "Please specifiy your email address<br />if you wish to receive a redownload link.",
  "de" => "Bitte geben Sie ihre Emailadresse an,<br />falls sie einen Link f&uuml;r erneute Downloads w&uuml;nschen."
);

$musichearts_text_no_javascript = array(
  "en" => "YOU NEED JAVASCRIPT ENABLED TO USE THIS MUSICHEARTS SHOP.<br />PLEASE ENABLE.",
  "de" => "SIE BRAUCHEN JAVASCRIPT UM DIESEN MUSICHEARTS SHOP ZU BENUTZEN.<br />BITTE EINSCHALTEN."
);

$musichearts_text_no_cookies = array(
  "en" => "YOU NEED COOKIES ENABLED TO USE THIS MUSICHEARTS SHOP. PLEASE ENABLE.",
  "de" => "SIE BRAUCHEN COOKIES UM DIESEN MUSICHEARTS SHOP ZU BENUTZEN. BITTE EINSCHALTEN."
);

$musichearts_text_problem_mail = array(
  "en" => "Problems? Send an Email to ",
  "de" => "Probleme? Schicken Sie eine Email an "
);

$musichearts_text_no_auth = array(
  "en" => "Sorry. You have no authorization to download this file:",
  "de" => "Sie haben leider keine Berechtigung diese Datei herunterzuladen:"
);

$musichearts_text_max_dl_reached = array(
  "en" => "Probably you have reached the maximum number of downloads.",
  "de" => "Wahrscheinlich haben Sie die maximale Anzahl an Downloads erreicht."
);

$musichearts_text_why_email = array(
  "en" => "Why specify email?",
  "de" => "Warum email angeben?"
);

$musichearts_text_confirm_no_email1 = array(
  "en" => "If you do not specify an email address, musichearts will not be able to send " .
          "you a link for 3 redownloads. ",
  "de" => "Wenn Sie keine Emailadresse angeben, kann Ihnen musichearts keinen Link f&uuml;r " .
          "3 weitere Downloads schicken. "
);

$musichearts_text_confirm_no_email2 = array(
  "en" => "Press -Cancel- if you want to return and specify an email address " .
          "or -Ok- to continue without." ,
  "de" => "Dr&uuml;cken Sie -Abbrechen- um zur&uuml;ckzukehren und ein Emailadresse anzugeben " .
          "oder -Ok- um ohne fortzufahren."
);

$musichearts_text_email_subject = array(
  "en" => "Your musichearts songs by ",
  "de" => "Ihre musichearts Lieder von "
);

$musichearts_text_email_content1 = array(
  "en" => "Hi\r\n\r\n" .
          "Thank you for your purchase.\r\n\r\n" .
          "Here are links for 3 additional downloads of your purchased songs:" ,
  "de" => "Hallo,\r\n\r\n" .
          "Vielen Dank f�r Ihren Einkauf.\r\n\r\n".
          "Hier sind Downloadlinks f�r 3 weitere Downloads Ihrer Songs:"
); 

$musichearts_text_email_content2 = array(
  "en" => "Have fun\r\n" .
          "Bye, ",
  "de" => "Viel Spass\r\n" .
          "Servus, "
); 

$musichearts_text_help_preview = array(
  "en" => "Click on this icon to preview a song.",
  "de" => "Klicken Sie auf dieses Symbol um ein Lied vorzuh&ouml;ren."
);

$musichearts_text_help_order = array(
  "en" => "Click this icon if you want to buy this song.",
  "de" => "Klicken Sie auf dieses Symbol, wenn Sie das Lied kaufen wollen."
);

$musichearts_text_help_download = array(
  "en" => "Right-Click this link and choose 'Save as...'  to download a song you have bought.",
  "de" => "Klicken Sie rechts auf diesen Link und w&auml;hlen Sie 'Speichern unter...' um ein gekauftes " .
          "Lied zu speichern."
);

$musichearts_text_payment_simulated = array(
  "en" => "Normally you would have to pay at paypal here.",
  "de" => "Normalerweise m&uuml;sste hier bei Paypal bezahlt werden."
);

$musichearts_text_install_not_done = array(
  "en" => "Shop is currently beeing installed. You cannot order yet.",
  "de" => "Laden wird gerade installiert. Es kann noch nicht bestellt werden."
);

$musichearts_text_link_shop = array(
  "en" => " - prelisten to MP3's and shop",
  "de" => " - MP3's probeh&ouml;ren und kaufen"
);


?>
