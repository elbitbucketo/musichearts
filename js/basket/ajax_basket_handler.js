
var musichearts_xmlhttp = null;
   
if (window.XMLHttpRequest)
{
  // code for IE7+, Firefox, Chrome, Opera, Safari
  musichearts_xmlhttp=new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
// code for IE6, IE5
  musichearts_xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
else
{
  musichearts_xmlhttp = null; 
}

if( musichearts_xmlhttp == null )
  alert("Fehler beim Erzeugen des Anfrage-Objekts!");



//Now the functions

function set_dl_link_style( style, do_notify )
{

  document.getElementById('musichearts_goto_downloads').style.visibility = style;
    
  if( do_notify )
    document.getElementById( "musichearts_notification" ).style.visibility = 'visible'; 

}
  


function button_basket_action( a_dom_obj, musichearts_root_dir, basket_key )
{
  //alert( a_dom_obj.onclick );
  a_dom_obj.onclick = function () { return false; };
  
  var song_task = null;
  var hex_song_name = a_dom_obj.id.split( ':', 2 )[1];
  var update_url = musichearts_root_dir + 'php/basket/update_basket_ajax.php';

  if( a_dom_obj.name == 'add:' + a_dom_obj.id )
  {
    song_task = 'add';
  }
  else if( a_dom_obj.name == 'remove:' + a_dom_obj.id )
  {
    song_task = 'remove';
  }
  else
  {
    alert( 'SORRY! Javascript error.' );
  }
  
  musichearts_xmlhttp.open( "POST", update_url, true );
  musichearts_xmlhttp.onreadystatechange = handle_basket_answer;
  musichearts_xmlhttp.setRequestHeader( "Content-Type", "application/x-www-form-urlencoded" );
  postdata = "song_task="   + song_task +
             "&song_key="   + hex_song_name +
             "&root_dir="   + musichearts_root_dir +
             "&basket_key=" + basket_key; 
  musichearts_xmlhttp.send( postdata );
}


function handle_basket_answer()
{
  if( musichearts_xmlhttp.readyState == 4 )
  {

    answer = musichearts_xmlhttp.responseText;
    //alert( answer );
    
    var hex_song_name            = answer.split( '|||', 6 )[0];
    var basket_price_sum         = answer.split( '|||', 6 )[1];
    var button_text              = answer.split( '|||', 6 )[2];
    var do_notify                = answer.split( '|||', 6 )[3];
    var root_dir                 = answer.split( '|||', 6 )[4];
    var basket_key               = answer.split( '|||', 6 )[5];

    a_dom_obj   = document.getElementById( 'hex:' + hex_song_name );
    img_dom_obj = document.getElementById( 'img:hex:' + hex_song_name );
    
    img_dom_obj.alt = button_text;
    if( a_dom_obj.name == 'add:' + a_dom_obj.id )
    {
      a_dom_obj.name  = 'remove:' + a_dom_obj.id; 
      img_dom_obj.src   = root_dir + 'png/checkbox_on.png';
    }
    else if( a_dom_obj.name == 'remove:' + a_dom_obj.id )
    {
      a_dom_obj.name  = 'add:' + a_dom_obj.id; 
      img_dom_obj.src   = root_dir + 'png/checkbox_off.png';
    }
    else
    {
      alert( 'SORRY! Javascript error.' );
    }
  
    document.getElementById('musichearts_basket_price_sum').innerHTML = basket_price_sum;

    if( basket_price_sum > 0 )
    {
      if( do_notify == 'YES' )
        set_dl_link_style( 'visible', true );
      else
        set_dl_link_style( 'visible', false );
    }
    else
    {
      set_dl_link_style( 'hidden', false );
    }
  
    a_dom_obj.onclick = function ( ) { button_basket_action( this, root_dir, basket_key ); return false; };
    //alert( a_dom_obj.onclick );
    
  }
}
