function musichearts_popup( url, title ) 
{

  if( !title )
    title = "musichearts";
    
  popup_win = window.open(
    url,
    title, 
    "width=300,height=300,status=yes,scrollbars=yes,resizable=yes"
  );
  
  popup_win.moveBy( 150, 100);

  popup_win.focus();
  
  return popup_win;
  
}
