

// Ensure that opera correctly display ajax dom modifications
// when using back/forward buttons
if( history.navigationMode )
  history.navigationMode = 'fast';
