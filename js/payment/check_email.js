function musichearts_check_email( confirm_no_email_text1, confirm_no_email_text2 )
{
  var answer    = true;
  var mail_addr = document.getElementById( 'musichearts_customer_email_address' ).value;
 

  if( !mail_addr )
    answer = confirm( confirm_no_email_text1 + '\r\n' + confirm_no_email_text2 );


  return answer;
}

