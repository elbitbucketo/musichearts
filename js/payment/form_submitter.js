  var xmlhttp_custmail = null;
   
  if (window.XMLHttpRequest)
  {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp_custmail=new XMLHttpRequest();
  }
  else if (window.ActiveXObject)
  {
  // code for IE6, IE5
    xmlhttp_custmail=new ActiveXObject("Microsoft.XMLHTTP");
  }
  else
  {
    xmlhttp_custmail = null; 
  }

  if( xmlhttp_custmail == null )
    alert("ERROR creating AJAX request object!");


function musichearts_store_email_address( musichearts_root_dir )
{

  var mail_addr = document.getElementById( 'musichearts_customer_email_address' ).value;

  if( mail_addr )
  {
    var update_url = musichearts_root_dir + 'php/email/update_email_ajax.php';
  
    xmlhttp_custmail.open( "POST", update_url, false );
    //xmlhttp_custmail.onreadystatechange = abc;
    xmlhttp_custmail.setRequestHeader( "Content-Type", "application/x-www-form-urlencoded" );
    postdata = "musichearts_customer_email_address=" + mail_addr;
    xmlhttp_custmail.send( postdata );
  }
  //alert( mail_addr );
}

/*function abc()
{
  if( xmlhttp_custmail.readyState == 4 )
  {
    answer = xmlhttp_custmail.responseText;
    alert ( answer );
  }
}*/


function musichearts_submit_form( local_development, current_base_url, simulation_text )
{
  
  //form = document.getElementById( 'musichearts_payment' );
  //alert( form.id );
  // TODO: solve dynamically by name
  // TODO: activate code when more than 1 payment plugin
  
  //if( document.getElementById('radio_musichearts_payment_paypal').checked )
  if( local_development == null )
    document.getElementById('musichearts_payment_paypal').submit();
  else
  {
    alert( simulation_text );
    window.location = current_base_url; 
  }

}

