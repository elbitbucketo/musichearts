// checking if cookies are enabled
// if not show an error and deny shop access
function check_cookie()
{
  // check if the cookie has been set and has correct value
  var a_all_cookies = document.cookie.split( ';' );
  var a_temp_cookie = '';
  var cookie_value = '';

  for ( i = 0; i < a_all_cookies.length; i++ )
  {
    // split into name=value pairs
    a_temp_cookie = a_all_cookies[i].split( '=' );

    if ( a_temp_cookie[0].replace( /^\s+|\s+$/g, '') == 'musichearts_test' )
    {
      if ( a_temp_cookie.length > 1 )
        if( unescape( a_temp_cookie[1].replace( /^\s+|\s+$/g, '') == '123test' ) )
          return true;
        else
          return false;
      else
        return false;
    }
    a_temp_cookie = null;
  }
  return false;
}
  
document.cookie = 
  'musichearts_test=123test' +
  ';expires=' + ( new Date( ( new Date ).getTime() + 1000 * 60 * 60 * 24 * 365 ) ).toGMTString() +
  ';path=/;';
  //( (domain) ? ";domain=" + domain : "") +
  //( (secure) ? ";secure" : "");


if ( check_cookie() )
{
  // name, path, domain: same as above
  document.cookie = 
    'musichearts_test=123test' +
    ';expires=Thu, 01-Jan-70 00:00:01 GMT'
    ';path=/;';
    //( (domain) ? ";domain=" + domain : "") +
    //( (secure) ? ";secure" : "");
  document.getElementById('musichearts_cookie_error').style.height     = '0em';
  document.getElementById('musichearts_cookie_error').style.visibility = 'hidden';
  document.getElementById('musichearts_shop_table').style.visibility   = 'visible';
}
else
{
  document.getElementById('musichearts_cookie_error').style.visibility = 'visible';
  document.getElementById('musichearts_cookie_error').style.height     = '150px';
  document.getElementById('musichearts_shop_table').style.visibility   = 'hidden';
}
