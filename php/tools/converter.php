<?php

  // CLASS converter provides methods for conversion
  //       e.g. string <-> hexdata
  class musichearts_converter
  {
  
    //////////////////
    // METHOD SECTION

    static function hex2string( $hex )
    {
      $string='';
      for( $i = 0; $i < strlen( $hex ) - 1; $i += 2 )
      {
          $string .= chr( hexdec( $hex[ $i ].$hex[ $i+1 ] ) );
      }
      return $string;
    }
    

    static function string2hex( $string )
    {
      $hex='';
      for( $i = 0; $i < strlen( $string ); $i++ )
      {
          $hex .= dechex( ord( $string[ $i ] ) );
      }
      return $hex;
    }


  }


?>
