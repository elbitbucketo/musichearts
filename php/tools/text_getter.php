<?php

  // serverside script for getting multilingual text
  // synchronous javascript

  chdir( '../..' );
  include_once 'txt/common_texts.php';
  include_once 'php/multilingual/text_provider.php';


  // Output encoded results back to javascript
  echo musichearts_text::get ( $_POST['text_key'] ) ;

?>
