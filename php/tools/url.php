<?php

  class musichearts_url_tools
  {
    
    public static function get_current_url( $base_only = false, $no_get_data = true )
    {
    // return the current URL
    // if $base_only is set, only return the current dir on webserver
    
      $return_url = 'http';
      if( isset( $_SERVER["HTTPS"] ) && $_SERVER["HTTPS"] == "on" ) 
        $return_url .= "s";
      $return_url .= "://";
      if ($_SERVER["SERVER_PORT"] != "80")
        $return_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
      else 
        $return_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
      
      $return_url = preg_replace( '/^(.*)\?.*$/', '$1', $return_url );

      if( $base_only )
        $return_url = preg_replace( '/^(.*)\/.*$/', '$1', $return_url );
      if( $no_get_data )
        $return_url = preg_replace( '/^.*\?(.*)$/', '$1', $return_url );
      
      return $return_url;
     
      // OLD IMPL
      //return URL for PDT should be set in paypal profile as paypal states
      //$return_url = 'http';
      //if ($_SERVER["HTTPS"] == "on") 
        //$return_url .= "s";
      //$return_url .= "://";
      //if ($_SERVER["SERVER_PORT"] != "80")
        //$return_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
      //else 
        //$return_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
      //$return_url = preg_replace( '/\?.*$/', '', $return_url);
      
    }

  }

?>
