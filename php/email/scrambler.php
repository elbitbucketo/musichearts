<?php

  final class email_scrambler
  {
    static public function encode_email_address( $plain_email_address )
    {
    // scramble email address to protect users from spam
      return str_replace(
        '@',
        '___at___',
        str_replace(
          '.',
          '___dot___',
          $plain_email_address
        )
      );
    }
  }

?>
