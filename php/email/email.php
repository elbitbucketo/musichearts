<?php

class musichearts_email
{

   private static $to_addr_array = null;
   private static $cc_addr_array = null;
   
   private static $subject = '';
   private static $content = '';
   
   
   public function add_recipient( $address, $cc = false )
   {
     if( !$cc )
       self::$to_addr_array[] = $address;
     else
       self::$cc_addr_array[] = $address;
   }
   
   
   public function set_content( $content )
   {
   // TODO: only 72 chars per line allowed
   //       cd. php.net mail() handbook page
     self::$content = $content;  
   }
   
   
  public function set_subject( $subject )
   {
     self::$subject = $subject;  
   }
   
   
   public function send()
   {
     global $musichearts_band_email;
     global $musichearts_non_rfc_mail_headers;
   
     $to      = '';
     $header  = '';
     
     if( $musichearts_non_rfc_mail_headers )
       $header_lb = "\n";
     else
       $header_lb = "\r\n";  
     
     // build to: and cc: address strings
     if( is_array( self::$to_addr_array  ) )
     {
       $first_addr = true;
       foreach( self::$to_addr_array as $to_addr )
       {
         if( !$first_addr )
           $to = $to . ', ';
         $to = $to . $to_addr;
         $first_addr = false;
       }
     }
     if( is_array( self::$cc_addr_array  ) )
     {
       $first_addr = true;
       foreach( self::$cc_addr_array as $cc_addr )
       {
         if( !$first_addr )
           $cc = $cc . ', ';
         $cc = $cc . $cc_addr;
         $first_addr = false;
       }     
     }

     // Maybe look for \r\n problem on php.net mail() page
     // have some tests with win and xampp first though
     $header = 'From: '. $musichearts_band_email . $header_lb .
               'X-Mailer: PHP/' . phpversion();
     if( isset( $cc ) ) 
       $header .= $header_lb . 'Cc: ' . $cc;

     return mail( 
       $to , 
       self::$subject, 
       self::$content,
       $header
     );

   }
   
   

}

?>
