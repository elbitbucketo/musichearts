<?php

  // CLASS musichearts_text provied access methods for consistent
  //                        multilingual webpage desing
  class musichearts_text{

    /////////////////////
    // ATTRIBUTE SECTION

    private static $language = null;

    
    //////////////////
    // METHOD SECTION

    public static function get( $id )
    {
      // since this function is called from various locations,
      // it includes without error messages the text files
      // TODO: Maybe chdir() now works also due to better structure
      // anyhow this is not really important, it works just fine :-)
      // already fixed???
      include 'txt/common_texts.php';
      ////////@include '../../txt/common_texts.php';

      if( !isset( self::$language ) ) 
        self::set_browser_language();

      $full_id = 'musichearts_text_'.$id;
      
      if( isset( ${$full_id}[ self::$language ] ) )
        return ${$full_id}[ self::$language ];

      else if( isset( $musichearts_text_missing[ self::$language ] ) )
        return $musichearts_text_missing[ self::$language ];

      else
        return '#######################';

    }
    
    
    private static function set_browser_language()
    {
      // NOTE: This function is a modified copy.
      //       Original under BSD license.
      //       Here is the original license text:
      /*
        Copyright (c) 2008 Darrin Yeager                        
        http://www.dyeager.org/                               
        Licensed under BSD license.                           
        http://www.dyeager.org/downloads/license-bsd.php    
      */
      global $musichearts_default_language; 
      $deflang =  $musichearts_default_language; 

      if(isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]) && strlen($_SERVER["HTTP_ACCEPT_LANGUAGE"]) > 1)  {
         // Split possible languages into array
         $x = explode(",",$_SERVER["HTTP_ACCEPT_LANGUAGE"]);
         foreach ($x as $val) {
            //check for q-value and create associative array. No q-value means 1 by rule
            if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
               $lang[$matches[1]] = (float)$matches[2];
            else
               $lang[$val] = 1.0;
         }
   
         //return default language (highest q-value)
         $qval = 0.0;
         foreach ($lang as $key => $value) {
            if ($value > $qval) {
               $qval = (float)$value;
               $deflang = $key;
            }
         }
      }

      self::$language = substr( strtolower($deflang), 0, 2 );
    }
  

  }

?>
