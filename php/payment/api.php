<?php

  
  interface musichearts_payment_api
  {
    public static function get_description();
    
    // e-payment data usually is submitted by forms specified by
    // payment provider
    // musichearts hides this from the user and perform the work
    // in background
    public static function get_hidden_form();
    public static function get_hidden_form_id();
        
    public static function check_payment( $data, $basket );  
  }



  // CLASS musichearts_payment provides the API for payment operations
  abstract class musichearts_payment implements musichearts_payment_api
  {
    
    //////////////////
    // METHOD SECTION

    public static final function get_payment_methods()
    {
      
      global $musichearts_paypal_username;
      if( !empty( $musichearts_paypal_username ) ) 
        $payment_methods[] = 'musichearts_payment_paypal';
        // next: click and buy?
      
      return $payment_methods;
    
    }
  
  }

  
?>
