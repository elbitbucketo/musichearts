<?php

// TODO: prefix
final class db_access_sqlite implements musichearts_db_access
{

  private static $handle = null;




  private static function open()
  {
    $error_message = null;

    self::$handle = sqlite_open(
      'db/musichearts.sqlite',
      0666,
      $error_message
    );
    if( !isset( self::$handle ) || isset( $error_message ) )
    {
      self::$handle = null;
      throw new musichearts_exception( 2, $error_message );
    }

    $tables = sqlite_fetch_array( sqlite_query( self::$handle, 'SELECT name FROM sqlite_master WHERE type="table"') );
    
    if(    !is_array( $tables  )
        || !in_array( 'download_tokens', $tables )
    )
      sqlite_exec(
        self::$handle,
        'CREATE TABLE download_tokens ' .
                      '(id TEXT, filename TEXT, downloads INTEGER, PRIMARY KEY ( id ) )' 
      ); 


  }
  


  public static function close()
  {
    self::$handle = null;
  }



  public static function insert( $query )
  {
   if( !isset( self::$handle ) )
     self::open();

    return @sqlite_exec(
      self::$handle,
      $query
    );
  }



  public static function update( $query )
  {
   if( !isset( self::$handle ) )
     self::open();
    
    return @sqlite_exec(
      self::$handle,
      $query
    );
  }



  public static function select( $query )
  {
   if( !isset( self::$handle ) )
     self::open();
  
    return sqlite_fetch_single(
      sqlite_query(
        self::$handle,
        $query
      )
    );
  }
  
  
  
  public static function delete( $query )
  {
   if( !isset( self::$handle ) )
     self::open();
    
    return @sqlite_exec(
      self::$handle,
      $query
    );
  }  


}


?>
