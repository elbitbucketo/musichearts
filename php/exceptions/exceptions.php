<?php
/**
 * Define a custom exception class
 */
class musichearts_exception extends Exception
{

    // Redefine the exception so code isn't optional
    public function __construct( $code, $message = null )
    {
        parent::__construct( $message, $code );
    }

}
?>
