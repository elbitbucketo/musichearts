<?php

  // CLASS central_plugin coordinates between UI and misc plugins APIs
  //                      ( AJAX stuff bypasses this, but does not access
  //                        any APIs anyhow )
  final class musichearts_central_plugin
  {
  
    /////////////////////
    // ATTRIBUTE SECTION

    private static $song_access_plugin;


    //////////////////
    // METHOD SECTION

    public static final function get_songs_from_plugin()
    {
      return call_user_func( array( self::get_configured_song_plugin(), 'get_songs' ) ); 
    }


    
    public static final function get_free_songs_from_plugin()
    {
      foreach( self::get_songs_from_plugin() as $song )
      {
         if( $song->price > 0 )
           continue;
         $songs[] = $song;
      }
      return $songs;
    }



    public static final function get_song_from_plugin( $hex_song_name )
    {
      return call_user_func( array( self::get_configured_song_plugin(), 'get_song' ), $hex_song_name ); 
    }
    
    
    
    public static final function get_song( $hex_song_name )
    {
    // TODO: find calls and remove this function since this is only a forward
      return self::get_song_from_plugin( $hex_song_name );
    }


    
    public static final function get_preview_song( $song_filename )
    {
      $song = call_user_func( 
        array( 
          self::get_configured_song_plugin(),
          'get_preview_song' 
        ), 
        $song_filename
      );
      return $song;
    }



    private static final function get_configured_song_plugin()
    {
      if( self::$song_access_plugin == null )
      {
        //TODO: later on config will tell which kind of plugin
        $song_access_plugin ='musichearts_song_access_filebased';
      }
      return $song_access_plugin;
    }
    

  }

?>
