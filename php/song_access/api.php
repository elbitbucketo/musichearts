<?php

  interface musichearts_song_access_api
  {
  // TODO: Check for more functions in here
    public static function get_songs();
    
    public static function get_song( $hex_song_name );
    
    public static function get_preview_song( $preview_song_filename );
  }
  


  // CLASS musichearts_song_access provides the API for access operations
  //                               related to song files
  abstract class musichearts_song_access implements musichearts_song_access_api{
    
    /////////////////////
    // ATTRIBUTE SECTION

    private static $song_access_plugin;
    private static $song_object_cache; // songs have to be cached in some situations
                                       // to access current data before a set cookie
                                       // is available. (after payment)


    //////////////////
    // METHOD SECTION

    //public abstract static function get_songs();
    
   

    //public static abstract function get_preview_song( $preview_song_filename );



    public static final function int_get_preview_song( $preview_song_filename )
    {
      return new musichearts_preview_song( $preview_song_filename );
    }

    
    
    public static final function update_song_cache( $song )
    {
      $same_song_in_cache = null;

      if( is_array( self::$song_object_cache ) )
      {
        foreach( self::$song_object_cache as &$cache_song )
        {
          if( $cache_song->filename != $song->filename )
            continue;
          $same_song_in_cache = &$cache_song;
        }
        if( is_object( $same_song_in_cache ) )
          $same_song_in_cache = $song;
        else
          self::$song_object_cache[] = $song;
      }
      else
        self::$song_object_cache[] = $song;
    }



    public static final function get_song_from_cache( $hex_song_name )
    {
      $song = null;

      if( is_array( self::$song_object_cache ) )
      {
        foreach( self::$song_object_cache as &$cache_song )
        {
          if( $cache_song->filename != musichearts_converter::hex2string( $hex_song_name ) )
            continue;
          $song = &$cache_song;
        }
      }
      return $song;
    }



    public static final function get_paid_songs_from_cache()
    {
      $songs = null;
      if( is_array( self::$song_object_cache ) )
      {
        foreach( self::$song_object_cache as &$cache_song )
        {
          if( $cache_song->check_payment() == true )
            $songs[] = &$cache_song;
        }
      }
      return $songs;
    }


    
    public static final function get_song( $hex_song_name )
    {
      $song = self::get_song_from_cache( $hex_song_name );
      
      if( !is_object( $song ) )
      {
        $song = new musichearts_full_song( musichearts_converter::hex2string( $hex_song_name ) );
        if( is_object( $song ) )
          self::update_song_cache( $song );
        else
          die( '!!!ERROR!!! inconsistent song DB !!!ERROR!!!' );
      }

      return $song;
    }



  }
  
?>
