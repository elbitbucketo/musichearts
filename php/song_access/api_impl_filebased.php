<?php


  // CLASS musichearts_song_access_filebased implements file access api using just
  //                                         the server side file system and no DBs
  class musichearts_song_access_filebased extends musichearts_song_access
  {

    //////////////////
    // METHOD SECTION
    private static final function get_sorted_filenames()
    {
      global $musichearts_musicfiles_dirname;
      
      $filedir = opendir( $musichearts_musicfiles_dirname ) 
                 or die( 'TODO FIXME Fehler' );
      while( $filename = @readdir($filedir) )
      {
        if(    !is_dir( $musichearts_musicfiles_dirname.'/'.$filename ) 
            && preg_match( '/\.[Mm][Pp]3$/', $filename ) 
        )
        // TODO: File endings may be configured
        // TODO: Subdirectories as albums
          $songnames[] = $filename;
      }
      sort( $songnames );
      
      return $songnames;
    }



    public static function get_songs()
    {
      foreach( musichearts_song_access_filebased::get_sorted_filenames() as $filename )
      {
        $song = self::get_song( musichearts_converter::string2hex( $filename ) );
        $songs[ musichearts_converter::string2hex( $song->filename ) ] = $song;
        //parent::update_song_cache( $song );
      }
      
      return $songs;
    }



    public static function get_preview_song( $filename )
    {
      global $musichearts_musicfiles_preview_dirname;

      // check is a preview song exists and return name if yes
      $preview_song_filename = preg_replace( '/\.mp3$/', '.preview.mp3', $filename );
      if( 
        file_exists( 
          $musichearts_musicfiles_preview_dirname.'/'.$preview_song_filename 
        ) 
      )
        return self::int_get_preview_song( $preview_song_filename );
      else
        return null;
    }


  }


?>
