<?php
  
  // CLASS 'basket' represents the shopping basket
  class musichearts_basket
  {

    //////////////////////
    // ATTRIBUTE SECTION
    
    private $hex_song_names; // array with hex converted songnames (=song object keys) 
                             // hex encoding is needed so that the encoded songnames
                             // do correctly validate in html-id/-name tags

    
    ///////////////////
    // METHOD SECTION
    
    public function remove_song( $hex_song_name )
    {
      // check if song is in basket, if yes remove
      if( $this->is_in_basket( $hex_song_name ) )
      {
        $hex_song_name_remove = array( $hex_song_name );
        $this->hex_song_names = array_diff( $this->hex_song_names, $hex_song_name_remove );
      }
    }



    public function add_song( $hex_song_name )
    {
      // check if song is in basket, if not add
      if( !$this->is_in_basket( $hex_song_name ) )
      {
        $this->hex_song_names[] = ( $hex_song_name );
      }
    }



    public function is_in_basket( $hex_song_name )
    {
      // check if a song is already in basket
      
      if( is_array( $this->hex_song_names ) )
        return in_array( $hex_song_name , $this->hex_song_names );
      else
        return false;
    }



    public function get_song_objects()
    {
      // return all songs in basket
      $songs = null;

      if( is_array( $this->hex_song_names ) )
      {
        foreach( $this->hex_song_names as $hex_song_name )
          $songs[]  = musichearts_central_plugin::get_song_from_plugin( $hex_song_name );
      }
      
      //TODO: sort
      return $songs;
    }

    

    public function get_basket_price()
    { 
      // calculate the total sum of all song prices in basket an return it
      
      $basket_price = 0;
      if( is_array( $this->hex_song_names ) )
        foreach( $this->hex_song_names as $hex_song_name )
          $basket_price += musichearts_central_plugin::get_song_from_plugin( $hex_song_name )->price;
      
      return $basket_price;
    }
    

    
    public function is_empty()
    {
      // tell if basket is empty
      return empty( $this->hex_song_names );
    }

  }

?>
