<?php

  // serverside script for updating basket via
  // asynchronous javascript

  chdir( '../..' );
  include 'html/include_top.html.php';


  // get basket from session
  if( array_key_exists( $_POST['basket_key'], $_SESSION ) && is_object( $_SESSION[ $_POST['basket_key'] ] ) )
  {
    $basket = $_SESSION[ $_POST['basket_key'] ];
  }
  else
  {
    $basket = new musichearts_basket();
  }

  if( isset( $_SESSION['musichearts_notify'] ) )
    $_SESSION['musichearts_notify'] = 'NO';

  // analyse request and add or remove song
  if( $_POST['song_task'] == 'add' )
  {
    if( $basket->is_empty() && !isset( $_SESSION['musichearts_notify'] ) )
      $_SESSION['musichearts_notify'] = 'YES';
    
    $basket->add_song( $_POST['song_key'] );
    $button_text = musichearts_text::get( 'unget' );
  }
  else if( $_POST['song_task'] == 'remove' )
  {
    $basket->remove_song( $_POST['song_key'] );
    $button_text = musichearts_text::get( 'get' );
  }

 
  // Output encoded results back to javascript
  echo $_POST['song_key']                              . '|||' .
       number_format( $basket->get_basket_price(), 2 ) . '|||' .
       $button_text                                    . '|||' .
       $_SESSION['musichearts_notify']                 . '|||' .
       $_POST['root_dir']                              . '|||' .
       $_POST['basket_key'];
 
?>
