<?php
  
  chdir( '../..' );
  //session_cache_limiter("must-revalidate");  
  //seems not to be needed anymore due to having moved this fix into page header

  include @constant('musichearts_root_dir').'html/include_top.html.php';

// Obtain GET data and store in variables
  if( array_key_exists( 'file' , $_GET ) )
    $file = rawurldecode( $_GET['file'] );
    
  if( array_key_exists( 'token' , $_GET ) )
    $token_id = rawurldecode( $_GET['token'] );
    
  if( array_key_exists( 'musichearts_dl_mimetype' , $_GET ) )
    $mimetype = rawurldecode( $_GET['musichearts_dl_mimetype'] );
  else
    $mimetype = 'application/force-download';
    #$mimetype = 'audio/mpeg';


  if( isset( $file ) )
  {
    
    $song = new musichearts_full_song( $file ); 

    // TODO: CHECK IF SONG DATA IS CONSISTENT
    
    if( $song->check_download_permission_cookie() )
    {
      $auth_okay = true;
    }
    else if( isset( $token_id ) && $song->check_download_permission_token( $token_id ) )
    {
      $auth_okay = true;
      $song->book_download( $token_id );
    }
    else
      $auth_okay = false;
      
    if( $auth_okay == true )
    {
    // authentification correct -> enable download
    
      $file_full = $musichearts_musicfiles_dirname.'/'.$file;

      // IE seems to need the disabling of caching in some (server?) configurations
      header( 'Expires: -1' );
      header( 'Cache-Control: post-check=0, pre-check=0' );
      header( 'Pragma: no-cache' );
      header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
      header( 'Content-Type: '.$mimetype );
      header( 'Content-Transfer-Encoding: Binary' );
      header( 'Content-Length: '.filesize( $file_full ) );
      header( 'Content-Disposition: attachment; filename="'.$file.'"' );
    
      readfile( "$file_full" );

    }
    else
    {
    // authentification failed
    
      include 'html/page_top.html.php';
?>
      <div style="color: #000000; background-color: #ffffff; border: 5px solid red; padding: 20px;">
        <br />
<?php
        echo musichearts_text::get( 'no_auth' ) . "\n";
?>
        <br />
<?php
        echo $file . "\n";
        if( isset( $token_id ) )
        {
        // additional message if token access has failed
?>
          <br />
          <br />
<?php
          echo musichearts_text::get( 'max_dl_reached' ) . "\n";
        }
?>
        <br />
        <br />
      </div>
<?php
      include 'html/page_bottom.html.php';
      
    }

  }
  else
  {
  // normal flow should not end up here!
  // if i end here something seriously went wrong
    echo '!!!ERROR!!! No file selected - Check referer !!!ERROR!!!';
  }

?>
