<?php

// TODO: This should be cleaned...
//       song object shows some strange growing


  // CLASS song represents one song
  abstract class musichearts_song
  {
   
    /////////////////////
    // ATTRIBUTE SECTION
    public $filename;

   
    /////////////////////
    // METHOD SECTION
    public function __construct( $filename )
    {
      $this->filename = $filename;
      // TODO: Check if the song exists... (via plugin)
      //       Not important now, because only filebased plugin exists
      //       and songs always exist using this plugin.
      //       Will be more important for mysql plugin.
    }

  }



  // CLASS song represents one free preview song
  final class musichearts_preview_song extends musichearts_song
  {
    
    /////////////////////
    // METHOD SECTION
    public function __construct( $preview_filename )
    {
      parent::__construct( $preview_filename );
    }

  }


  // CLASS song represents one full song which may be purchased
  final class musichearts_full_song extends musichearts_song
  {
    
    /////////////////////
    // ATTRIBUTE SECTION
    public $preview_song;
    public $price;

    private $is_paid;
  


   
    /////////////////////
    // METHOD SECTION
    public function __construct( $filename )
    {
      global $musichearts_song_price;

      $purchased_songs = null;


      $this->price = $musichearts_song_price;
      
      parent::__construct( $filename );

      // check is a preview song exists and fill attribute if yes
      $this->preview_song = musichearts_central_plugin::get_preview_song( $this->filename );
      
      // check if this song has already been purchased before
      if( isset( $_COOKIE[ 'musichearts_ps' ] ) )
        $purchased_songs = explode( '|||', $_COOKIE[ 'musichearts_ps' ] );
      if(    is_array( $purchased_songs ) 
          && in_array( musichearts_converter::string2hex( $this->filename ), $purchased_songs, true )
      )
        $this->is_paid = true;
      
    }
  


    
    public function confirm_payment( $make_token_link = true )
    {
      
      global $musichearts_cookie_download_time;
      $purchased_songs = null;


      $this->is_paid = true;

      // Store the payment confirmation in a 'always' lasting cookie
      // so the customer can alway redownload his songs
      if( isset( $_COOKIE[ 'musichearts_ps' ] ) )
         $purchased_songs = explode( '|||', $_COOKIE[ 'musichearts_ps' ] );

      if(    !is_array( $purchased_songs )
          || !in_array( musichearts_converter::string2hex( $this->filename ), $purchased_songs, true )
      )
      {
        $purchased_songs[] = musichearts_converter::string2hex( $this->filename );
   
        // Ensure that cookie always contains all cached song so they can be downloaded
        // right away
        foreach( musichearts_song_access::get_paid_songs_from_cache() as $song )
          if( $song->filename != $this->filename )
            $purchased_songs[] = musichearts_converter::string2hex( $song->filename );

        setcookie( 
          'musichearts_ps', 
          implode( '|||', $purchased_songs ),
          time() + 60 * 60 * 24 * $musichearts_cookie_download_time
        );
      }
       
       
       if( $make_token_link )
       {
         $download_link = $this->provide_download_link();

         musichearts_song_access::update_song_cache( $this );

         return $download_link;
       }

    }




    public function check_payment()
    {
      return $this->is_paid;
    }




    private function provide_download_link()
    {
      // provide a link with download key
      $inserted = false;
      while( $inserted == false )
      {
        $token_id = substr( rawurlencode( md5( microtime() ) ), 0, 10 );
        $check_select = db_access_sqlite::select( 
          'SELECT count( id ) FROM download_tokens WHERE id = "'. $token_id .'"' 
        ); 
        if( $check_select == "0" )
        {
          $query = 'INSERT INTO download_tokens ( id, filename, downloads ) '.
                          'VALUES ( "'. $token_id .'", "'. $this->filename .'", 3 )'            ;
          // TODO: configurable download count
          $result = db_access_sqlite::insert( 
            $query
          ); 
          $inserted = true;
        }
      }
      
      return musichearts_url_tools::get_current_url( true ) . 
             "/" . 
             @constant('musichearts_root_dir') .
             "php/download/download.php" .
             "?file=" . 
             rawurlencode( $this->filename ).
             "&token=" . 
             $token_id;

    }
  


  
    public function check_download_permission_cookie()
    {
      $purchased_songs = null;

      if( is_array( $_COOKIE ) && array_key_exists( 'musichearts_ps', $_COOKIE ) )
        $purchased_songs = explode( '|||', $_COOKIE[ 'musichearts_ps' ] );
    
      if(    is_array( $purchased_songs ) 
          && in_array( musichearts_converter::string2hex( $this->filename ), $purchased_songs, true )
      )
        return true;
      else
        return false;
    
    }
    
    
    
    
    public function check_download_permission_token( $token_id )
    {
    
      if( !isset( $token_id ) )
        return false;
        
      $available_downloads = db_access_sqlite::select( 
        'SELECT downloads FROM download_tokens ' .
               'WHERE     id = "'. $token_id .'" ' .
                     'AND filename = "' . $this->filename . '" '.
                     'AND downloads > 0' 
      );
      if( isset( $available_downloads ) && $available_downloads > 0 )
        return true;
      else
        return false;
    
    }    
    
  


    public function book_download( $token_id )
    {
        $available_downloads = db_access_sqlite::select( 
          'SELECT downloads FROM download_tokens ' .
                 'WHERE     id = "'. $token_id .'" ' .
                       'AND filename = "' . $this->filename . '" '.
                       'AND downloads > 0' 
        );
        $available_downloads--;
        if( $available_downloads > 0 )
          db_access_sqlite::update( 
            'UPDATE download_tokens '.
                   'SET downloads = '. $available_downloads . ' ' .
                   'WHERE     id = "'. $token_id .'" ' .
                         'AND filename = "' . $this->filename . '" '
          );
        else
          db_access_sqlite::delete( 
            'DELETE FROM download_tokens '.
                   'WHERE     id = "'. $token_id .'"' . 
                         'AND filename = "' . $this->filename . '" '
          );      
                
    }  
 


  }
  
?>
