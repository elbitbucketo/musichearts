<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>musichearts install checker</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<?php 
  chdir( '..' );
  include_once 'config.php';
  include_once 'txt/common_texts.php';
  include_once 'php/exceptions/exceptions.php';
  #include_once 'php/tools/converter.php';  
  include_once 'php/tools/url.php';
  include_once 'php/multilingual/text_provider.php';
  include_once 'php/db/api.php';
  include_once 'php/db/api_impl_sqlite.php';
  include_once 'php/email/email.php';
  #include_once 'php/email/scrambler.php';
  #include_once 'php/payment/api.php';  
  #include_once 'php/payment/api_impl_paypal.php';  
  #include_once 'php/songs/songs.php';
  #include_once 'php/basket/basket.php';
  #include_once 'php/song_access/api.php';
  #include_once 'php/song_access/api_impl_filebased.php';  
  #include_once 'php/central_plugin/central_plugin.php';
  $shop_url = musichearts_url_tools::get_current_url( true );
  $shop_url = preg_replace( '/tools$/', '', $shop_url ). "index.php";
  $shop_link = '<a href="' . 
                 $shop_url . 
                 '" onclick="musichearts_win = window.open( this.href, \'musichearts\', \'top=40,left=40,width=760,height=440,scrollbars=yes,resizable=yes\' ); musichearts_win.focus(); return false;">' . 
                 $musichearts_band_name . musichearts_text::get('link_shop') . 
               '</a>';
?>
<body style="font-family: Courier New, Courier, monospace; font-weight: bold; font-size: 110%;">
<div style="margin: 30px">
  <br /> 
  <?php
    if( !$musichearts_complete_html_page )
    {
  ?>
      <span style="color: red; font-size: 120%;">
        You need to set '$musichearts_complete_html_page = true' in config.php in order to be able to use the
        link creator.
      </span>
  <?php
    }
    else
    {
  ?>
      Your shop is available under the URL '<?php echo "$shop_url"; ?>'.
      <br />
      <br />
      You can easily embed it into any page by copy/pasting the code below into it:
      <br />
      <br />
      <div style="background-color: lightgrey; border: 5px solid darkgrey; padding: 20px; font-weight: bold;">
  <?php
        echo str_replace( ">", "&gt;", str_replace( "<", "&lt;", $shop_link ) );
  ?>
      </div>
      <br /> 
      <br /> 
      To test how it would work, just click the link below:
      <br /> 
      <br /> 
      <?php echo $shop_link; ?>
      <br /> 
      <br /> 
      The design will of course fit into you webpage.
  <?php
    }
  ?>  
</div>
</body>
</html>
