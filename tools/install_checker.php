<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>musichearts install checker</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
</head>
<?php 
  chdir( '..' );
  include_once 'config.php';
  include_once 'php/exceptions/exceptions.php';
  include_once 'php/tools/url.php';
  include_once 'php/db/api.php';
  include_once 'php/db/api_impl_sqlite.php';
  include_once 'php/email/email.php';

  function musichearts_checker_error( &$error )
  {

    if( !isset( $error ) )
    {
      echo '<span style="color: green; font-weight: bold;">OK</span><br />';
    }
    else
    {
      echo '<span style="color: red; font-weight: bold;">ERROR</span>';
      foreach( $error as $error_line )
        echo '<br /><span style="color: red;">&nbsp;&nbsp;&nbsp;- ' . $error_line . '</span>';
      $error = null;
    }

  }
  
  function musichearts_checker_skip( &$skip )
  {

    if( !isset( $skip ) )
      return false;

    echo '<span style="color: orange; font-weight: bold;">SKIPPED</span>';
    foreach( $skip as $skip_line )
      echo '<br /><span style="color: orange;">&nbsp;&nbsp;&nbsp;- ' . $skip_line . '</span>';

    return true;
  }

?>
<body style="font-family: Courier New, Courier, monospace; font-weight: bold; font-size: 110%;">
<div>
<br />
musichearts now is checking your installation:<br />
==============================================
<br />
<br />

1. Checking basic configuration settings...............<?php
  // A. Check paypal data
  if( $musichearts_paypal_username == '__FILL_ME__' || $musichearts_paypal_auth_token == '__FILL_ME__' )
    $error[] = 'Paypal data not entered into config.php.';
  // B. Check band data
  if( $musichearts_band_name == '__FILL_ME__' || $musichearts_band_email == '__FILL_ME__@__FILL__.__ME__' )
  {
    $error[]     = 'Band data not entered into config.php.';
    $skip_mail[] = 'Enter your artist data first.';
  }
  
  musichearts_checker_error( $error );
?>

<br />
<br />

2. Checking database access and setup..................<?php
  // B. Check if sqlite db can be written, read and deleted
  try
  {
    @db_access_sqlite::delete( 'DELETE FROM download_tokens WHERE id = "XXXXXXXXXX" AND filename = "YYYYYYYYYY"' ) ;
    if( !db_access_sqlite::insert( 'INSERT INTO download_tokens ( id, filename, downloads ) VALUES ( "XXXXXXXXXX", "YYYYYYYYYY", 0 )' ) ) 
      $error[] = 'Cannot write database. Check setup.';
    if( db_access_sqlite::select( 'SELECT count( id ) FROM download_tokens WHERE id = "XXXXXXXXXX"' ) == 0 )
      $error[] = 'Cannot read database. Check setup.';
    if( !db_access_sqlite::delete( 'DELETE FROM download_tokens WHERE id = "XXXXXXXXXX" AND filename = "YYYYYYYYYY"' ) ) 
      $error[] = 'Cannot delete from database. Check setup.';
  }
  catch( musichearts_exception $exception )
  {
    $error[]     = 'Cannot access database. Check setup. (' . $exception->getMessage() . ')';
    $skip_mail[] = 'Setup your database correctly first.';
  }

  musichearts_checker_error( $error );
?>
<br />
<br />

3. Checking download mail capabilities.................<?php
  if( !musichearts_checker_skip( $skip_mail ) )
  {
    // A. Sending Email to see if feature is working.
    //    Check for mail address from external needed
    //    NOT NOW... Checking with band email first
    
          //TODO: MAIL WITH DOWNLOAD LINKS IN SUBFUNCTION
          $email = new musichearts_email();

          $email->add_recipient( $musichearts_band_email );

          $email->set_subject(
            'musichearts mp3 shop: test'
          );
    
          $email->set_content( 'If you received this email, musichearts email feature is working.' ); 
  
          $result = $email->send();
    ?><span id="mh_mail" style="color: orange;"></span>
    <span id="mh_mail_err" style="color: red; visibility: hidden; height: 0px;">
      <br />
      &nbsp;&nbsp;&nbsp;- Mail cannot be sent from within musichearts.
    </span>
    <script  type="text/javascript">
        var answer    = true;

        if( confirm( 
          'Please check inbox of "<?php echo $musichearts_band_email;?>" for new email.\r\n\r\n' +
          'Press "OK" if you have received an email with subject "musichearts mp3 shop: test".\r\n'
        ) )
        {
          document.getElementById( 'mh_mail' ).style.color='green';
          document.getElementById( 'mh_mail' ).innerHTML='OK';
        }
        else
        {
          document.getElementById( 'mh_mail' ).style.color='red';
          document.getElementById( 'mh_mail' ).innerHTML='ERROR';
          document.getElementById( 'mh_mail_err' ).style.visibility='visible';
        }
      </script>
    <?php
  }
?>
<br />
<br />

4. Checking security of directory 'mp3'................<?php
  // A. Check access is denied by webserver
  $tsturl = musichearts_url_tools::get_current_url( true ) . '/../mp3/index.html';
  $handle = @fopen( $tsturl, "r" );
  if( $handle )
    $error[] = 'mp3 directory can be read from external.';
  
  musichearts_checker_error( $error );
?>
<br />
<br />

5. Checking security of directory 'db'.................<?php
  // A. Check access is denied by webserver
  $tsturl = musichearts_url_tools::get_current_url( true ) . '/../db/index.html';
  $handle = @fopen( $tsturl, "r" );
  if( $handle )
    $error[] = 'db directory can be read from external.';
  
  musichearts_checker_error( $error );
?>
<br />
<br />
</div>
</body>
</html>
