<?php

include_once 'config.php';

if( $musichearts_complete_html_page == true )
    include 'html/include_top.html.php';

$musichearts_basket_key =
    "musichearts_basket_" . md5(  musichearts_url_tools::get_current_url() );

// get basket from session
if(
    isset( $_SESSION[ $musichearts_basket_key ]  ) &&
    is_object( $_SESSION[ $musichearts_basket_key ] )
)
    $basket = $_SESSION[ $musichearts_basket_key ];
else
    $basket = new musichearts_basket();
// write basket back to session
$_SESSION[ $musichearts_basket_key ] = &$basket;

if( $musichearts_complete_html_page == true )
    include 'html/page_top.html.php';

if( isset( $_GET['tx'] ))
{
    include 'html/include_payment_callback.html.php';
    ob_end_flush();
}

if( isset( $_GET['musichearts_payment'] ))
    include 'html/include_payment.html.php';
else
    include 'html/include_musichearts.html.php';

if( $musichearts_complete_html_page == true )
    include 'html/page_bottom.html.php';
//if( isset( $_GET['tx'] ))
//ob_end_flush();

?>
