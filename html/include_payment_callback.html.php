<?php
    
    if( isset( $_GET['tx'] ) )
    {
      // here in know i am in paypal, no further checks needed
      try
      {
        musichearts_payment_paypal::check_payment( $_GET['tx'], $basket );
      }
      catch( musichearts_exception $musichearts_exception ) 
      {
?>
          <p>
            <h3>
              <?php echo musichearts_text::get( 'cannot_purchase' ); ?>
            </h3>
<?php
            echo $musichearts_exception->getMessage();
?>
          </p>
<?php
      }
    }
?>
