<?php
  chdir( '..' );
  
  include_once 'config.php';
  include_once 'txt/common_texts.php';
  include_once 'php/multilingual/text_provider.php';
  
  include 'html/page_top_popup.html.php';

?>
    <br />
    <img alt="musichearts_help" src="../sndman/image/icon_play.png" class="musichearts_no_border" />
    &nbsp;<?php echo musichearts_text::get( 'help_preview' ); ?>
    <br />
    <br />
    <img alt="musichearts_help" src="../png/checkbox_off.png" class="musichearts_no_border" />
    &nbsp;<?php echo musichearts_text::get( 'help_order' ); ?>
    <br />
    <br />
    <div>
      <a href="#" onclick="False;"><?php echo musichearts_text::get( 'download' ); ?></a>
      &nbsp;<?php echo musichearts_text::get( 'help_download' ); ?>
    </div>
<?php
  include 'html/page_bottom_popup.html.php';
?>
