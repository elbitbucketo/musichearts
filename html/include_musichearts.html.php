    <!-- IE<=6 just won't show this popup, i am not willing to fix it for em -->
    <!--[if gte IE 7]><!--> 
      <?php include 'html/include_musichearts_notification.html.php'; ?>
    <!--<![endif]-->
    <noscript>
      <div id="musichearts_js_error" class="musichearts_red_warning">
      <br />
        <p>
          <?php echo musichearts_text::get( 'no_javascript' ); ?>
        </p>
      </div>
    </noscript>
    <div id="musichearts_cookie_error" class="musichearts_red_warning musichearts_hidden">
    <br />
      <p>
        <?php echo musichearts_text::get( 'no_cookies' ); ?>
      </p>
    </div>
    <div id="musichearts_shop_table" >
      <?php if( $musichearts_show_logo ) { ?>
        <a href="http://musichearts.org" class="musichearts_link" >
          <img class="musichearts_w3c" src="<?php echo @constant('musichearts_root_dir'); ?>png/musichearts_logo.png" alt="the musichearts logo" />
        </a>
      <?php } ?>
      <h3>
        <?php echo $musichearts_band_name; ?> - musichearts <?php echo musichearts_text::get( 'shop' ); ?>
      </h3>
      <?php
        if( !$musichearts_install_finished )
        {
      ?>
        <div class="musichearts_red_warning">
          <?php echo musichearts_text::get( 'install_not_done' ); ?>
        </div>
      <?php
        }
      ?>
      
      <!-- INCLUDING THE MAIN TABLE -->
      <?php include 'html/include_musichearts_table.html.php'; ?>
     
      <?php
        if( $musichearts_install_finished || $musichearts_development_mode )
        {
      ?>
      <div id="musichearts_goto_downloads">
        <input type="button" class="musichearts_download" value="<?php echo musichearts_text::get( 'pay' ); ?>" onclick="parent.location='?musichearts_payment=YES'" />
      </div>
      <?php
        } else {
      ?>
      <div id="musichearts_goto_downloads">
        <input type="button" class="musichearts_download" value="<?php echo musichearts_text::get( 'pay' ); ?>" onclick="alert( '<?php echo musichearts_text::get( 'install_not_done' ) ; ?>' );" />
      </div>
      <?php
        }
      ?>

      <br />
      <a href="mailto:" class="musichearts_link" id="musichearts_problem_email_link" >
        dummy
      </a>
    </div>
    <script type="text/javascript" > 
      <?php if( $basket->get_basket_price() > 0 )
        {
          // TODO: Ajax may be used for more intelligent pop-up behaviour.
      ?>
          set_dl_link_style( 'visible', false );
      <?php } else { ?>
          set_dl_link_style( 'hidden', false );
      <?php 
        }
      ?>
    </script>
    <script  type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/checks/javascript.js" >
    </script>
    <script  type="text/javascript" src="<?php echo @constant('musichearts_root_dir'); ?>js/checks/cookies.js" >
    </script>
    <script  type="text/javascript" >
      // Following code protects customers email.
      // Email is not written into page source in cleartext. (as it shouldn't be)
      document.getElementById( 'musichearts_problem_email_link').href = musichearts_provide_email_address( 
        '<?php echo email_scrambler::encode_email_address( $musichearts_band_email ); ?>',
        'chief___at___musichearts___dot___org',
        false 
      ); 
      document.getElementById( 'musichearts_problem_email_link').innerHTML =
        '<?php echo musichearts_text::get( 'problem_mail' ) ?>\'' 
        + musichearts_provide_email_address(
          '<?php echo email_scrambler::encode_email_address( $musichearts_band_email ); ?>',
          'chief___at___musichearts___dot___org' , 
          true 
        )
        +'\'';      
    </script>
