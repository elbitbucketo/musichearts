    <div class="musichearts_center">
      <br />
      <br />
      <a name="musichearts_pay_anchor" />
      <br />
      <?php

        if( !is_array( $basket->get_song_objects() ) )
        {
      ?>
          <?php echo musichearts_text::get( 'empty_basket' ); ?>
          <br />
          <a href="?" >
              <?php echo musichearts_text::get( 'back_to_shop' ); ?>
          </a>
      <?php
        }
        else
        {

          foreach( musichearts_payment::get_payment_methods() as $payment_method )
          {
            foreach( call_user_func( array( $payment_method, 'get_hidden_form' ) ) as $html_line )
            {
              echo "\n      ";
              echo $html_line;
            }
            echo "\n";
          }
        ?>
          <h2>
            <?php
              echo musichearts_text::get( 'amount_to_pay' )        . ' ' .
                   number_format( $basket->get_basket_price(), 2 ) . ' ' .
                   $musichearts_song_currency;
            ?>
            <br />
            <br />
          </h2>
          <p>
            <?php echo musichearts_text::get( 'redownload_link' ) ?> 
            <br />
            <input name="musichearts_customer_email_address" id="musichearts_customer_email_address" type="text" size="26" maxlength="100" class="musichearts_customer_email_address" />
            <br />
            <a href="<?php echo @constant('musichearts_root_dir'); ?>html/info_email_redownload.html.php" onclick="musichearts_popup( this.href ); return false;" >
              <?php echo musichearts_text::get( 'why_email' ) ?>
            </a>
            <br />
            <br />
          </p>
          <?php
            foreach( musichearts_payment::get_payment_methods() as $payment_method )
            {
            ?>
              <div class="musichearts_payment_section">
                <!-- TODO: activate when more than 1 payment option 
                     TODO: If more payment methods make default configurable
                <input
                  type="radio"
                  name="radio_<?php echo call_user_func( array( $payment_method, 'get_hidden_form_id' ) ); ?>"
                  id="radio_<?php echo call_user_func( array( $payment_method, 'get_hidden_form_id' ) ); ?>"
                  value="<?php echo call_user_func( array( $payment_method, 'get_description' ) ); ?>"
                  checked="checked"
                />
                &nbsp;&nbsp;&nbsp;
                -->
                <a href="" onclick=" javascript:window.open( 'https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside', 'olcwhatispaypal', '  toolbar=no, location=no, directories=no, status=no, ' + 'menubar=no, scrollbars=yes, resizable=yes, width=401, height=350'); return false; " >
                  <img  src="https://www.paypal.com/en_US/i/bnr/horizontal_solution_PPeCheck.gif" alt="Paypal Solution Graphics" class="musichearts_no_border" />
                </a>
                <br />
              </div>
            <?php
            }
          ?>
          <div class="musichearts_center">
            <input type="button" class="musichearts_submit_payment" value="<?php echo  musichearts_text::get( 'submit_payment' ); ?>" onclick="
                if( 
                  musichearts_check_email(
                    '<?php echo musichearts_text::get( 'confirm_no_email1' ); ?>',
                    '<?php echo musichearts_text::get( 'confirm_no_email2' ); ?>'
                  ) 
                )
                {
                  musichearts_store_email_address( '<?php echo @constant('musichearts_root_dir'); ?>' ); 
                  musichearts_submit_form(
                    <?php 
                      if( $musichearts_development_mode_local ) 
                        echo 'true, \'' . 
                        musichearts_url_tools::get_current_url( false, true ) . 
                        '?tx=musichearts_local_test' .
                        '\', \'' .
                        musichearts_text::get( 'payment_simulated' ) .
                        '\''; 
                    ?>
                  )
                }
                else
                  return false;
            " />
          </div>
          <br />
          <br />
          <table class="musichearts_all musichearts_payment_overview">
            <thead>
              <tr>
                <th class="musichearts_payment_overview" scope="col">
                  <?php echo musichearts_text::get( 'song_list' ); ?>
                </th>
              </tr>
            </thead>
            <tbody>
            <?php
              // TODO: check if basket objekt exists
              foreach( $basket->get_song_objects() as $song ) 
              { 
            ?>
                <tr>
                  <td class="musichearts_all musichearts_payment_overview"> 
                    <?php echo $song->filename; ?>
                  </td>
                </tr>
            <?php 
              } 
            ?>
            </tbody>
          </table>
          <br />
          <br />
          <br />
      <?php 
        } 
      ?>
    </div>
