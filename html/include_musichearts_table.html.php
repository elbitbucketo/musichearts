        <table class="musichearts_all musichearts_basic_all" >
          <thead>
            <tr>
              <th scope="col" class="musichearts_basic_th musichearts_basic_th12" >
                <?php echo musichearts_text::get( 'song' ); ?>
              </th>
              <th scope="col" class="musichearts_basic_th musichearts_basic_th12" >
                <?php echo musichearts_text::get( 'price' ); ?>
              </th>
              <th scope="col" class="musichearts_basic_th musichearts_basic_th3" >
              </th>
              <th scope="col" class="musichearts_basic_th musichearts_basic_th4" >
                <a href="<?php echo @constant('musichearts_root_dir'); ?>html/info_help.html.php" onclick="musichearts_popup( this.href ); return false;" >
                  <img alt="musichearts_help" src="<?php echo @constant('musichearts_root_dir'); ?>png/help.png" class="musichearts_no_border" />
                </a> 
              </th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td class="musichearts_basic_td musichearts_basic_td1" >
                <?php echo musichearts_text::get( 'sum' ); ?>:
              </td>
              <td class="musichearts_basic_td musichearts_basic_td2" >
                <span id="musichearts_basket_price_sum">
                  <?php echo number_format( $basket->get_basket_price(), 2 ); ?>
                  <!-- TODO: Currencies with other than 2 digits e.g. YEN? -->
                </span>
                <?php echo $musichearts_song_currency; ?>
              </td>
              <td class="musichearts_all musichearts_basic_td musichearts_basic_td3" >
              </td>
              <td class="musichearts_all musichearts_basic_td musichearts_basic_td4" >
              </td>
              </tr>
          </tfoot>
          <tbody>
          <?php 
            $songs  = musichearts_central_plugin::get_songs_from_plugin();
            // TODO: Warn for no songs!!!
            $index = 0;
            foreach( $songs as $hex_song_name => $song ) 
            { 
              $index++;
          ?>
            <tr>
              <td class="musichearts_basic_td musichearts_basic_td1 <?php 
                  if( $index == 1 ) 
                    echo 'musichearts_basic_td_top'; 
                  else if( $index == count( $songs) ) 
                    echo 'musichearts_basic_td_bottom';
                  else 
                    echo 'musichearts_basic_td_middle'; 
              ?>" >
                <?php echo $song->filename; ?>
              </td>
              <td class="musichearts_basic_td musichearts_basic_td2 <?php 
                  if( $index == 1 ) 
                    echo 'musichearts_basic_td_top'; 
                  else if( $index == count( $songs) ) 
                    echo 'musichearts_basic_td_bottom';
                  else 
                    echo 'musichearts_basic_td_middle'; 
              ?>" >
                <?php echo number_format( $song->price, 2 ); ?>
                <!-- TODO: Currencies with other than 2 digits e.g. YEN? -->
                <?php echo $musichearts_song_currency; ?>
              </td>
              <td class="musichearts_basic_td musichearts_basic_td3 <?php 
                  if( $index == 1 ) 
                    echo 'musichearts_basic_td_top'; 
                  else if( $index == count( $songs) ) 
                    echo 'musichearts_basic_td_bottom';
                  else 
                    echo 'musichearts_basic_td_middle'; 
              ?>" >
                <div>
                <?php
                  if( is_object( $song->preview_song ) )
                  {
                ?>
                <ul class="graphic">
                  <li>
                    <a href="<?php echo @constant('musichearts_root_dir') .  $musichearts_musicfiles_preview_dirname .  '/' .  $song->preview_song->filename; ?>" class="sm2_link" >
                    </a>
                    </li>
                  </ul>
                <?php
                  }
                ?>
                </div>
              </td>
              <td class="musichearts_basic_td musichearts_basic_td4 <?php 
                  if( $index == 1 ) 
                    echo 'musichearts_basic_td_top'; 
                  else if( $index == count( $songs) ) 
                    echo 'musichearts_basic_td_bottom';
                  else 
                    echo 'musichearts_basic_td_middle'; 
              ?>" > 
              <?php
                if(    $song->price > 0
                    && $song->check_payment() != true
                )
                {
              ?>
                  <div>
                    <a 
                      href="#"
                      id="hex:<?php echo $hex_song_name; ?>"
                      class="musichearts_basket_inout"
                      onclick="
                        button_basket_action( 
                          this, 
                          '<?php echo @constant('musichearts_root_dir'); ?>',
                          'musichearts_basket_<?php echo md5(  musichearts_url_tools::get_current_url() );?>'
                        );
                        return false;
                      "
                    <?php
                      if( $basket->is_in_basket( $hex_song_name ) )
                      {
                    ?>
                        name="remove:hex:<?php echo $hex_song_name; ?>"
                    <?php
                      }
                      else
                      {
                    ?>
                        name="add:hex:<?php echo $hex_song_name; ?>"
                    <?php
                      }
                    ?>
                    >
                      <img
                        id="img:hex:<?php echo $hex_song_name; ?>"
                        class="basket_inout"
                    <?php
                      if( $basket->is_in_basket( $hex_song_name ) )
                      {
                    ?>
                        alt="<?php echo musichearts_text::get( 'unget' ); ?>"
                        src="<?php echo @constant('musichearts_root_dir'); ?>png/checkbox_on.png"
                    <?php
                      }
                      else
                      {
                    ?>
                        alt="<?php echo musichearts_text::get( 'get' ); ?>"
                        src="<?php echo @constant('musichearts_root_dir'); ?>png/checkbox_off.png"
                    <?php
                      }
                    ?>
                      />
                    </a>
                <?php
                  }
                  else
                  {
                    // song can be downloaded
                    ?>
                <div class="musichearts_dl_link" >
                  <a href="<?php echo @constant('musichearts_root_dir'); ?>php/download/download.php?file=<?php echo rawurlencode( $song->filename ); ?>" class="inline-exclude" >
                  <!--onclick="musichearts_popup( this.href ); return false;"-->
                    <?php echo musichearts_text::get( 'download' ); ?>
                  </a>
                <?php
                  }
                ?>
                </div>
              </td>
            </tr>
          <?php 
            } 
          ?>
        </tbody>
      </table>
