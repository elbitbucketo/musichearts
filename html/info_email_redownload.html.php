<?php
  chdir( '..' );
  
  include_once 'config.php';
  include_once 'txt/common_texts.php';
  include_once 'php/multilingual/text_provider.php';
  
  include 'html/page_top_popup.html.php';
  
  echo musichearts_text::get( 'confirm_no_email1' ); 

  include 'html/page_bottom_popup.html.php';
?>
