<?php
  
  include_once 'config.php';
  include_once 'txt/common_texts.php';
  include_once 'php/exceptions/exceptions.php';
  include_once 'php/tools/converter.php';  
  include_once 'php/tools/url.php';
  include_once 'php/multilingual/text_provider.php';
  include_once 'php/db/api.php';
  include_once 'php/db/api_impl_sqlite.php';
  include_once 'php/email/email.php';
  include_once 'php/email/scrambler.php';
  include_once 'php/payment/api.php';  
  include_once 'php/payment/api_impl_paypal.php';  
  include_once 'php/songs/songs.php';
  include_once 'php/basket/basket.php';
  include_once 'php/song_access/api.php';
  include_once 'php/song_access/api_impl_filebased.php';  
  include_once 'php/central_plugin/central_plugin.php';

  /* TODO how in php 7x?
   * session_start();
   */

  if( isset( $_GET['tx'] ))
    ob_start();

?>
