<?php

header('Content-type: text/css');
include '../config.php';

print <<<_CSS

h1
{
  font:             $musichearts_font;
  font-size:        250%;
}

h2
{
  font:             $musichearts_font;
  font-size:        200%;
}

h3
{
  font:             $musichearts_font;
  font-size:        150%;
}

h4
{
  font:             $musichearts_font;
  font-size:        120%;
}


body.musichearts_all{
  font:             $musichearts_font;
  background-color: $musichearts_background_color;
  color:            $musichearts_color;
}


td.musichearts_floating_download
{
  /* TODO: Styling (with jpg?) */
  /* TODO: Still needed? */
  background-color: $musichearts_color;
  color:            $musichearts_background_color;
}


a.musichearts_link 
{
  background-color: $musichearts_background_color;
  color:            $musichearts_link_color;
}


input.musichearts_button
{
  /* TODO: Styling (with jpg?) */
  /* NOTE: button colors not supported in firefox */
  background-color: $musichearts_color;
  color:            $musichearts_background_color;
}


input.musichearts_floating_download
{
  /* TODO: Styling (with jpg?) */
  background-color: $musichearts_background_color;
  color:            $musichearts_color;
}


input.musichearts_download 
{
  /* TODO: Styling (with jpg?) */
  background-color: $musichearts_color;
  color:            $musichearts_background_color;
}


img.musichearts_w3c 
{
  border-style:     none;
  border-width:     0px;
  margin-left:      auto;
  margin-right:     auto;
}


table.musichearts_all 
{
  font:             $musichearts_font;
  background-color: $musichearts_background_color;
  color:            $musichearts_color;
  border-color:     $musichearts_border_color;
  font-size:        1.1em;
}

_CSS

?>
