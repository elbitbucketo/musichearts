<?php

header('Content-type: text/css');
include '../config.php';

print <<<_CSS


.musichearts_center
{
  text-align: center;
}


.musichearts_hidden
{
  visibility: hidden;
}


.musichhearts_margin_5px
{
  margin: 5px;
}


div.musichearts_popup
{
  font:             $musichearts_font;
  background-color: $musichearts_background_color;
  color:            $musichearts_color;
  font-size:        95%;
  text-align:       justify;
}


div.musichearts_payment_section
{
  margin-left:  auto; 
  margin-right: auto; 
  text-align:   center; 
}


img.musichearts_no_border
{
  border-width: 0px;
}


div.musichearts_preview_sndman
{
  margin-left: 12px;
  padding-bottom: 20px;
  padding-left: 12px;
  vertical-align: middle;
  margin-left:  auto; 
  margin-right: auto; 
  text-align:   center; 
}


input.musichearts_submit_payment
{
  padding:   8px 60px 8px 60px; 
  margin:    0px 0px 0px 0px; 
  font-size: 120%;
  width:     253px;
}


a.musichearts_basket_inout,
a.musichearts_basket_inout:link,
a.musichearts_basket_inout:active,
a.musichearts_basket_inout:visited, 
a.musichearts_basket_inout:hover
{
  border:  none;
  outline: none;
}


img.basket_inout
{
  border-style: none;
}


input.musichearts_customer_email_address
{
  padding: 3px 0px 3px 0px;
  margin:  6px 0px 6px 0px;
}


.musichearts_basic_all
{
  width:            ${musichearts_plugin_width}; 
  border-width:     1px;
  border-collapse:  collapse;
  text-align:       right;
  margin-right:     auto;
  margin-left:      auto;
  margin-top:       20px;
  margin-bottom:    20px;
}


#musichearts_shop_table
{
  text-align: center;
  visibility: hidden;
}

table.musichearts_payment_overview
{
  text-align:       left; 
  border-collapse:  collapse;
  margin-right:     auto;
  margin-left:      auto;
  margin-top:       20px;
  margin-bottom:    20px;
}

th.musichearts_payment_overview
{
  padding:          15px 0px 5px 5px;
  border-style:     solid;
  border-width:     1px;
  font-size:        115%;
  padding-right:    65px;
  padding-left:     65px;
}

td.musichearts_payment_overview
{
  padding-right:       5px;
  padding-left:        5px;
  padding-top:         15px;
  padding-bottom:      15px;
  border-left-style:   solid;
  border-right-style:  solid;
  border-top-style:    solid;
  border-bottom-style: solid;
  border-width:        1px;
  vertical-align:      top;
}

th.musichearts_basic_th
{
  border-width:     1px;
  border-style:     solid;
}

th.musichearts_basic_th12
{
  padding:          15px 5px 5px 0px;
  font-size:        115%;
}

th.musichearts_basic_th3
{
  border-top-style:       none;
  border-right-style:     none;
  padding:                0px 0px 0px 0px;
  margin:                 0px 0px 0px 0px;
}

th.musichearts_basic_th4
{
  border-top-style:    none;
  border-right-style:  none;
  border-left-style:   none;
  padding:             0px 0px 5px 0px;
  /*padding-top:         0px;
  padding-right:       100px;*/  
}

td.musichearts_basic_td
{
  border-width:        1px;
  border-style:        solid;
  vertical-align:      middle;
}

td.musichearts_basic_td_top
{
  border-top-style:    solid;
  border-bottom-style: dashed;
}

td.musichearts_basic_td_middle
{
  border-top-style:    dashed;
  border-bottom-style: dashed;
}

td.musichearts_basic_td_bottom
{
  border-top-style:    dashed;
  border-bottom-style: solid;
}

tfoot td.musichearts_basic_td1
{
  padding:             10px 5px 10px 5px;
  vertical-align:      top;
  text-align:          right;
  font-weight:         bold;
}

tfoot td.musichearts_basic_td2
{
  text-align:          right;
  padding:             10px 5px 10px 5px;
  font-weight:         bold;

}

tfoot td.musichearts_basic_td3
{
  border-right-style:  none;
  border-bottom-style: none;
  border-left-style:   solid;
  padding-top:         15px;
  padding-right:       5px;
  padding-bottom:      15px;
  padding-left:        5px;
}

tfoot td.musichearts_basic_td4
{
  border-right-style:  none;
  border-bottom-style: none;
  border-left-style:   none;
  padding-top:         15px;
  padding-right:       5px;
  padding-bottom:      15px;
  padding-left:        5px;
}

tbody td.musichearts_basic_td1
{
  border-right-style:  dashed;
  padding-right:       5px;
  padding-left:        5px;
}

tbody td.musichearts_basic_td2
{
  border-left-style:   dashed;
  border-right-style:  dashed;
  padding-right:       5px;
  padding-left:        5px;
  width:               100px;
}

tbody td.musichearts_basic_td3
{
  border-left-style:   dashed;
  border-right-style:  dashed;
  width:               60px;
  text-align:          center;
  margin-right:        auto;
  margin-left:         auto;
}

tbody td.musichearts_basic_td4
{
  border-left-style:   dashed;
  width:               60px;
  text-align:          center;
  margin-right:        auto;
  margin-left:         auto;
}

div.musichearts_dl_link
{
  padding-right: 10px;
  padding-left:  10px;
}

ul.graphic
{
  height: 32px;
}

input.musichearts_download 
{
  padding:   8px 60px 8px 60px; 
  margin:    0px 0px 0px 0px; 
  font-size: 120%;
}

#musichearts_notification
{
  position:   fixed;
  width:      200px;
  top:        1em;
  right:      1em;
  visibility: hidden;
  float:            left;
  color:            $musichearts_background_color;
  background-color: $musichearts_color;
  padding:          0px;
  border-width:     2px;
  border-style:     solid;
  border-color:     $musichearts_background_color;
}

div.musichearts_notification_inside
{
  color:            $musichearts_color;
  background-color: $musichearts_background_color;
  cursor:           pointer;
  float:            right; 
  font-weight:      bold; 
  width:            25px; 
  margin:           0px;
  text-align:       center; 
  border-width:     2px;
  border-style:     solid;
  border-color:     $musichearts_color;
}


#musichearts_js_error
{
  visibility: visible;
}


div.musichearts_red_warning
{
  font-weight:      bold; 
  font-size:        25px;
  text-align:       center; 
  color:            #ff0000;
  background-color: #ffffff;
}


_CSS

?>
